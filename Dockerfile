FROM nginx:1.16.0-alpine

# Create app directory and use it as working directory
RUN mkdir -p /app
WORKDIR /app

COPY . /app

RUN apk add --update nodejs npm

RUN npm install
RUN npm install react-scripts@3.0.1 -g
RUN npm run build --prod

RUN cp -r build/. /usr/share/nginx/html

COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

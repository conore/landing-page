import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import Navbar from './components/navbar/index.js'
import Routes from './router/Routes'
import Footer from './components/footer/index.js'

import * as serviceWorker from './serviceWorker'
import './styles/container.css'

ReactDOM.render(
    <BrowserRouter>
        <Navbar />
        <Routes />
        <Footer />
    </BrowserRouter>, 
    document.getElementById('app')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

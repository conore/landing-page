import React from "react"
import '../../../styles/home/intro.css'

export default function Intro(props) {

    return (
        <div id="intro">
            <div id="intro-title">
                <h1>Facilitez les réservations de vos adhérents</h1>
            </div>

            <div id="intro-subtitle">
                <h2>Le meilleur outil pour gérer les réservations et les séances de votre activité ou votre club de sport.</h2>
            </div>

            <div id="intro-btn">
                <div id="intro-video">
                    <a href="/">VOIR LA VIDÉO</a>
                </div>
                <div id="intro-start">
                    <a href="/">DÉMARRER</a>
                </div>
            </div>
        </div>
    )

}
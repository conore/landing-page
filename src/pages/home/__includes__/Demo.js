import React from "react"
import '../../../styles/home/demo.css'

export default function Demo(props) {

    return (
        <div id="demo">
            <div id="demo-title">
                <h1>Demandez une démonstration <span>gratuite</span></h1>
            </div>

            <div id="demo-subtitle">
                <h2>Notre équipe vous présentera notre outil lors d'un appel par téléphone ou Skype.</h2>
            </div>

            <div id="demo-btn">
                <div id="demo-ask">
                    <a href="/">DEMANDER UNE DEMO</a>
                </div>
            </div>
        </div>
    )

}
import React from "react"
import '../../../styles/home/download-app.css'

import AndroidImg from '../../../assets/img/stores/google-play-badge.png'
import IOSImg from '../../../assets/img/stores/app-store-badge.png'
import IphoneTemplate from '../../../assets/img/iphone-template.png'

export default function DownloadApp(props) {

    return (
        <div id="download">
            <div id="downlaod-app">
                <div id="downlaod-app-background">
                </div>
            </div>

            <div id="downlaod-app-container">
                <div id="downlaod-app-left">
                    <h3>Une application pour vos adhérents</h3>
                    <p>Ils peuvent réserver leurs séances en moins de 30 secondes !</p>

                    <div id="downlaod-app-btn">
                        <a href="https://play.google.com/store/apps" >
                            <img src={AndroidImg} className="download-app-img" />
                        </a>
                        <a href="https://www.apple.com/fr/ios/app-store/" >
                            <img src={IOSImg} className="download-app-img" id="download-app-img-ios"/>
                        </a>
                    </div>
                </div>

                <div id="downlaod-app-rigth">
                    <img src={IphoneTemplate} id="iphone-template" />
                </div>
            </div>
        </div>

    )

}
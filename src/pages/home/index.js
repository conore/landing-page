import React from "react"
import Intro from "./__includes__/Intro"
import DownloadApp from "./__includes__/DownloadApp"
import Demo from "./__includes__/Demo"

export default function Home(props) {

    return (
        <div id="home">
            <Intro />
            <DownloadApp />
            <Demo />
        </div>
    )

}
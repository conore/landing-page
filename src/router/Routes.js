import React from "react"
import { Switch, Route, Redirect } from "react-router-dom"

import Home from '../pages/home/index.js'
import Features from '../pages/features/index.js'
import Prices from '../pages/prices/index.js'
import Blog from '../pages/blog/index.js'

import About from '../pages/about/index.js'
import CGU from '../pages/cgu/index.js'
import Partners from '../pages/partners/index.js'
import Contact from '../pages/contact/index.js'

const Routes = () => (
    <Switch>
        <Route path="/fonctionnalites" component={Features}/>
        <Route path="/tarifs" component={Prices}/>
        <Route path="/blog" component={Blog}/>
        <Route path="/a-propos" component={About}/>
        <Route path="/conditions-generales" component={CGU}/>
        <Route path="/partenaires" component={Partners}/>
        <Route path="/contact" component={Contact}/>
        <Route path="/" component={Home}/>
        <Redirect exact from="*" to="/" />
    </Switch>
)

export default Routes
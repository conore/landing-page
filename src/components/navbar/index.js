import React from "react"
import { NavLink } from 'react-router-dom';
import SvgColor from 'react-svg-color'
import ConoreIcon from '../../assets/icons/ic-conore.svg'

import '../../styles/navbar/navbar.css'

export default function Navbar(props) {

    return (
        <nav id="navbar">
            <a href="/">
                <NavLink id="nav-home-icon" to="/">
                    <SvgColor svg={ConoreIcon} width={30} />
                    <h1>conore</h1>
                </NavLink>
            </a>

            <div id="nav-option">
                <NavLink activeClassName="nav-option-link-active" className="nav-option-link" to="/fonctionnalites">
                    Fonctionnalités
                </NavLink>
                <NavLink activeClassName="nav-option-link-active" className="nav-option-link" to="/tarifs">
                    Tarifs
                </NavLink>
                <NavLink activeClassName="nav-option-link-active" className="nav-option-link" to="/blog">
                    Blog
                </NavLink>
            </div>

            <div id="nav-login">
                <a href="https://app.conore.com/login" id="login">SE CONNECTER</a>
                <a href="https://app.conore.com/signup" id="register">S'INSCRIRE</a>
            </div>
        </nav>
    )

}
import React from "react"
import SvgColor from 'react-svg-color'
import ConoreIcon from '../../assets/icons/ic-conore-light.svg'

import '../../styles/footer/footer.css'

export default function Footer(props) {

    return (
        <footer id="footer">
            <a href="/">
                <div id="footer-home-icon">
                    <SvgColor svg={ConoreIcon} width={30}/>
                    <h1>conore</h1>
                </div>
            </a>

            <div id="footer-option">
                <a href="/a-propos" className="footer-option-link">A propos</a>
                <a href="/conditions-generales" className="footer-option-link">Conditions Générales</a>
                <a href="/partenaires" className="footer-option-link">Partenaires</a>
                <a href="/contact" className="footer-option-link">Contact</a>
            </div>
        </footer>
    )

}